#include "netwrap.h"

void str_echo(int s)
{
	long arg1, arg2;
	ssize_t n;
	char line[MAXLINE];

	while (1) {
		if ((n = Readline(s, line, MAXLINE)) == 0)
			return;

		if (sscanf(line, "%ld%ld", &arg1, &arg2))
			snprintf(line, sizeof(line), "%ld\n", arg1 + arg2);
		else
			snprintf(line, sizeof(line), "input error\n");

		n = strlen(line);
		Writen(s, line, n);
	}
}
void Echo(int s)
{
	ssize_t n;
	char buf[MAXLINE] = {0};

 again:
	while ((n = read(s, buf, MAXLINE)) > 0)
		Writen(s, buf, n);

	if (n < 0 && errno == EINTR)
		goto again;
	else if (n < 0)
		printf("Echo");

}

int main()
{
	int s, cfd;
	pid_t child;
	socklen_t clilen;
	struct sockaddr_in client, server;

	s = Socket(AF_INET, SOCK_STREAM, 0);

	memset(&server, 0, sizeof(server));
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = htonl(INADDR_ANY);
	server.sin_port = htons(SERVER_PORT);

	Bind(s, (struct sockaddr *)&server, sizeof(server));
	Listen(s, LISTENQ);

	while(1) {
		clilen = sizeof(client);
		cfd = Accept(s, (struct sockaddr *) &client, &clilen);

		switch ((child = fork())) {
		case -1:
                        /* An error occured! */
			perror("fork");
			close(cfd);
			break;
		case 0:
			close(s);
			/* Echo(cfd); */
			str_echo(cfd);
			exit(0);
			break;
		default:
			close(cfd);
			break;
		}
	}
	close(s);

	return 0;
}
