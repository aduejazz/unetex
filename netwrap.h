#include <netinet/in.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#define MAXLINE 1 << 10
#define SERVER_PORT 9877
#define LISTENQ 1 << 10

int Socket(int, int, int);
int Accept(int, struct sockaddr *, socklen_t *);
void Bind (int, const struct sockaddr *, socklen_t);
void Listen(int, int);
void Echo(int);

void Writen(int, void *, size_t);
char *Fgets(char *, int, FILE *);
ssize_t Readline(int, void *, size_t);
ssize_t readline(int, void *, size_t);
