#include "netwrap.h"

void str_cli(int s)
{
       char sendline[MAXLINE] = {0};
       char recvline[MAXLINE] = {0};

	while (Fgets(sendline, MAXLINE, stdin) != NULL) {
		Writen(s, sendline, strlen(sendline));

		if (Readline(s, recvline, MAXLINE) == 0) {
			printf("server finished prematurely\n");
			close(s);
			exit(-1);
		}

		if (fputs(recvline, stdout) == EOF)
			perror("fputs");
	}
}


int main(int argc, char *argv[])
{
	int s;
	struct sockaddr_in server;

	if (argv[1] == NULL) {
		puts("provide server address");
		exit(0);
	}

	s = Socket(AF_INET, SOCK_STREAM, 0);

	memset(&server, 0, sizeof(server));
	server.sin_family = AF_INET;
	server.sin_port = htons(SERVER_PORT);
	if (inet_pton(AF_INET, argv[1], &server.sin_addr) == 0) {
		perror("inet_pton");
		close(s);
		exit(-1);
	}

	if (connect(s, (struct sockaddr *) &server, sizeof(server)) < 0) {
		perror("connect");
		close(s);
		exit(errno);
	}

	str_cli(s);

	return 0;
}
