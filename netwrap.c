#include "netwrap.h"

static int read_cnt;
static char *rp;
static char buffer[MAXLINE];

int Socket(int domain, int type, int protocol)
{
	int fd;

	if ((fd = socket(domain, type, protocol)) < 0) {
		perror("socket");
		exit(fd);
	}
	return fd;
}

void Bind (int s, const struct sockaddr *sa, socklen_t l)
{
	if (bind(s, sa, l) < 0) {
		perror("bind");
		close(s);
		exit(-1);
	}
}

void Listen(int s, int backlog)
{
	char	*ptr;

	/*4can override 2nd argument with environment variable */
	if ( (ptr = getenv("LISTENQ")) != NULL)
		backlog = atoi(ptr);

	if (listen(s, backlog) < 0) {
		perror("listen error");
		close(s);
		exit(-1);
	}
}

int Accept(int s, struct sockaddr *sa, socklen_t *salenptr)
{
	int n;

 again:
	if ( (n = accept(s, sa, salenptr)) < 0) {
#ifdef	EPROTO
		if (errno == EPROTO || errno == ECONNABORTED)
#else
			if (errno == ECONNABORTED)
#endif
				goto again;
			else {
				perror("accept");
				close(s);
				exit(n);
			}
	}
	return n;
}

char *Fgets(char *ptr, int n, FILE *stream)
{
	char	*rptr = NULL;

	if ( (rptr = fgets(ptr, n, stream)) == NULL && ferror(stream))
		perror("fgets");

	return rptr;
}

static ssize_t read_wrap(int fd, char *p)
{
	if (read_cnt <= 0) {
	again:
		if ((read_cnt = read(fd, buffer, sizeof(buffer))) < 0) {
			if (errno == EINTR)
				goto again;
			return -1;
		} else if (read_cnt == 0)
			return 0;
		rp = buffer;
	}
	read_cnt--;
	*p = *rp++;
	return 1;
}

ssize_t readline(int fd, void *p, size_t maxlen)
{
	ssize_t n, rc;
	char c, *ptr;

	ptr = p;
	for (n = 1; n < maxlen; n++) {
		if ((rc = read_wrap(fd, &c)) == 1) {
			*ptr++ = c;
			if (c == '\n')
				break;
		} else if (rc == 0) {
			*ptr = 0;
			return n - 1;
		} else
			return -1;
	}
	*ptr = 0;
	return n;
}

ssize_t Readline(int fd, void *ptr, size_t maxlen)
{
	ssize_t	n;

	if ( (n = readline(fd, ptr, maxlen)) < 0)
		printf("readline: %ld bytes", n);

	return n;
}

ssize_t readlinebuf(void **p)
{
	if (read_cnt)
		*p = rp;
	return read_cnt;
}

ssize_t readn(int fd, void *p, size_t n)
{
	size_t nleft;
	ssize_t nread;
	char *ptr;

	ptr = p;
	nleft = n;

	while (nleft > 0) {
		if ((nread = read(fd, ptr, nleft)) < 0) {
			if (errno == EINTR)
				nread = 0;
			else
				return -1;
		} else if (nread == 0)
			break;

		nleft -= nread;
		ptr += nread;
	}
	return (n - nleft);
}

ssize_t writen(int fd, const void *p, size_t n)
{
	size_t nleft;
	ssize_t nwritten;
	const char *ptr;

	ptr = p;
	nleft = n;

	while (nleft > 0) {
		if ((nwritten = write(fd, ptr, nleft)) <= 0) {
			if (nwritten < 0 && errno == EINTR)
				nwritten = 0;
			else
				return -1;
		}

		nleft -= nwritten;
		ptr += nwritten;
	}
	return n;
}

void Writen(int fd, void *ptr, size_t nbytes)
{
	if (writen(fd, ptr, nbytes) != nbytes)
		printf("writen not complete\n");
}
