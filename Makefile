CC=gcc

CFLAGS=-Wall #-mavx2 -g

# the build target executable:
CLIENT=client
SERVER=server

all: $(CLIENT) $(SERVER)

$(CLIENT): $(CLIENT).c
	$(CC) $(CFLAGS) -o $(CLIENT) $(CLIENT).c netwrap.c

$(SERVER): $(SERVER).c
	$(CC) $(CFLAGS) -o $(SERVER) $(SERVER).c netwrap.c

clean:
	$(RM) $(CLIENT) $(SERVER)
